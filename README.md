## Penyelesaian Soal Shift 2 Sistem Operasi

## Kelompok D05

| Nama | NRP |
| ------ | ------ |
| DHAFIN ALMAS NUSANTARA | 5025201064 |
| NEISA HIBATILLAH ALIF | 5025201170 |
| FEBERLIZER EDNAR WILLIAM GULTOM|05111940007004|

## ---SOAL 1---
Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 


## Soal a

Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).


Code:
```
void pertama() {
int status = 0;
pid_t child_id;

child_id = fork();

char *download[][8] = {{ "wget", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O", "Weapon.zip", NULL},{ "wget", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O", "Character.zip", NULL}};

char *ekstrak[][5] = {{ "unzip", "Weapon.zip", NULL},{"unzip", "Character.zip", NULL}};

if (child_id==0){
for (int i=0;i<2;i++){
if (fork()==0) continue;
execv ("/usr/bin/wget",download[i]);
}
}
else{
while ((wait(&status)) > 0);
for (int z=0;z<2;z++){
if (fork()==0) continue;
execv ("/usr/bin/unzip",ekstrak[z]);
}
}
}

```

Penjelasan: 
- Dengan perintah wget untuk mendoenload database dan unzip untuk membuka folder yang di zip lalu menggunakan wait untuk menunggu perintah sebelumnya selesai lalu dilakukan perintah selanjutnya dibagian childnya. 


Dokumentasi hasil pengerjaan:
-

Kendala yang dialami:
- Masih Bingung untuk randomize ambil data dari database dan membuat sistem gacha yang dimaksud soal

## ---SOAL 2---

Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review, tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun untuk menyelesaikan pekerjaannya.



## Soal a

Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder `/home/[user]/shift2/drakor`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.


Code :
```
char filename[]="drakor.zip";
char path[]="/home/dhafin/shift2/drakor";
	
void unzipfile()
{
	
	id_t child_id;
	int status;
	
	if((child_id=fork())==0)
	{
		execlp("mkdir","mkdir","-p",path,NULL);
	}
	
	while ((wait(&status))>0);
	
	if((child_id=fork())==0)
	{
		execlp("unzip","unzip","-qq",filename,"-d",path,NULL);
	}
	while ((wait(&status))>0);
}

```
Penjelasan :
- membuat folder baru shift 2 untuk tempat tujuannya dan selanjutnya melakukan unzip di folder tersebut

## Soal b dan c

Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam `/drakor/romance`, jenis drama korea action akan disimpan dalam `/drakor/action` , dan seterusnya.

Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.
Contoh: “/drakor/romance/start-up.png”.


Code :
```
void rmfile(){
   struct dirent *dp;
   DIR *folder;
   folder = opendir(path);

   if(folder != NULL){ 
       while((dp = readdir(folder)) != NULL){
            if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
                 if(dp->d_type == DT_DIR){
                 
			char pathrm[400];
			sprintf(pathrm, "%s/%s", path, dp->d_name);

			id_t child_id;
			int status;
			
			if((child_id=fork())==0)
			{
				execlp("rm","rm","-r",pathrm,NULL);
			}
			
			while ((wait(&status))>0)
			;
                 }
            }
       }
   }
}

```
Penjelasan :
- membuat folder sesuai dengan genre yang ada dalam file zip dengan `strcmp` dengan membandingkan nama yang sama maka akan dibuat file dengan nama yang sama. lalu akan memindahkan ke dalam folder tersebut.



## Soal d dan e

Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)

Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). 


Code :
```
void kategori(char *arrpath, char *filename)
{
	id_t child_id;
	int status;
	char *judul = strtok(filename, ";");
	char *tahun = strtok(NULL, ";");
	char *genre = strtok(NULL, ";");
	char *genrenopng = strtok(genre, ".");
	
	char pathkategori[250],pathfilename[250],pathtxt[250];
	
	sprintf(pathkategori, "%s/%s", path, genrenopng);
	sprintf(pathfilename, "%s/%s/%s.png", path, genre,judul);
	sprintf(pathtxt, "%s/%s/data.txt", path, genre);
	
	
	if((child_id=fork())==0)
	{
		execlp("mkdir","mkdir","-p",pathkategori,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	if((child_id=fork())==0)
	{
		execlp("cp","cp",arrpath,pathfilename,NULL);
	}
	
	while ((wait(&status))>0)
	;
	
	char kategoritxt[300];
	char isitxt[300];
	sprintf(isitxt, "kategori : %s \nnama : %s\nrilis : tahun %s\n\n", genre, judul,tahun);
	
	FILE *filetxt;
	filetxt = fopen(pathtxt, "a");
	
	if(filetxt) {
        fprintf(filetxt, "%s", isitxt);
        fclose(filetxt);
    }
}
```

```

 if(folder != NULL){ 
       while((dp = readdir(folder)) != NULL){
            if(strcmp(dp->d_name, ".")&& strcmp(dp->d_name, "..") ){
                 if(dp->d_type == DT_REG){
                      char arrpath[400], filename[400];
                      sprintf(arrpath, "%s/%s",path, dp->d_name);
                      sprintf(filename, "%s",  dp->d_name);
                      

                      char *drakor1 = strtok(filename, "_");
                      char *drakor2 = strtok(NULL, "_");
                      if(drakor2 != NULL){                      
                          kategori(arrpath, drakor2);
                      }
                      kategori(arrpath,drakor1);

                      pid_t pid;
                      pid = fork();
                      if(pid == 0){
                         char *argv[] = {"rm", "-rf", arrpath, NULL};
                         execv("/usr/bin/rm", argv);  
                      }
                      while(wait(NULL) != pid);
                 }
            }

```
Penjelasan :
- digunakan strtok untuk memisah nama file dari judul,tahun rilis dan genrenya. setelah itu dibuat folder sesuai nama genre yang didapat dari semua file menggunakan mkdir.Selanjutnya untuk soal (e) dibuat file .txt untuk menyimpan data poster disetiap foldernya.

Kendala yang dialami:
- Masih bingung dan asing dengan istilah istilah syntax string string yang seharusnya efektif digunakan dalam bahasa c dan diaplikasikan pada modul 2 khususnya. 


## ---SOAL 3---
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang.

## Inisiasi
Untuk soal nomor 3, diperlukan inisiasi beberapa variabel. Untuk mengambil nama user, digunakan syntax berikut.
```
char *user;
user = (char *)malloc(10*sizeof(char));
user = getlogin();
```

Untuk mengambil path yang diperlukan, digunakan syntax sebagai berikut.
```
char home[100]; 
strcpy(home, "/home/");
strcat(home, user);

char modul2[100];
strcpy(modul2, home); 
strcat(modul2, "/modul2");

char zip[100];
strcpy(zip, home); 
strcat(zip, "/animal.zip");

char animal[100]; 
strcpy(animal, modul2);
strcat(animal, "/animal");

char darat[100];
strcpy(darat, modul2);
strcat(darat, "/darat");

char air[100];
strcpy(air, modul2);
strcat(air, "/air"); 

char daratmv[100];
strcpy(daratmv, animal);
strcat(daratmv, "/*darat*");

char airmv[100];
strcpy(airmv, animal);
strcat(airmv, "/*air*");

char bird[100];
strcpy(bird, darat);
strcat(bird, "/*bird*");

char list[100]; 
strcpy(list, air); 
strcat(list, "/list.txt");
```

## Soal a
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di `/home/[USER]/modul2/` dengan nama `darat` lalu 3 detik kemudian membuat directory ke 2 dengan nama `air`.

Code:
```
char *argv[] = {"mkdir", "-p", darat, NULL};
execv("/bin/mkdir", argv);
// ...
sleep(3);
// ...
char *a[] = {"mkdir", "-p", air, NULL};
execv("/bin/mkdir", a);
```

Penjelasan: 
- `mkdir -p` digunakan untuk membuat directory baru jika belum ada dan mengabaikannya jika sudah ada.
- `sleep` digunakan untuk menunggu selama sekian detik.

## Soal b
Kemudian program diminta dapat melakukan extract `animal.zip` di `/home/[USER]/modul2/`.

Karena akan melakukan extract, sebelumnya perlu diinstal package yang diperlukan.
```
sudo apt install unzip
```

Asumsikan `animal.zip` telah didownload pada path `/home/user/animal.zip`.

Code:
```
char *b[] = {"unzip", "-q", zip, "-d", modul2, NULL};
execv("/usr/bin/unzip", b);
```

Penjelasan: 
- `unzip -q` digunakan untuk mengekstrak file dengan tidak mencetak nama file keluaran yang diekstrak.
- `-d` digunakan untuk memindahkan hasilnya ke directory lain.

## Soal c
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder `/home/[USER]/modul2/darat` dan untuk hewan air dimasukkan ke folder `/home/[USER]/modul2/air`. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

Code:
```
glob_t globbuf;
globbuf.gl_offs = 2;
glob(daratmv, GLOB_DOOFFS, NULL, &globbuf);
glob(darat, GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);
globbuf.gl_pathv[0] = "mv";
globbuf.gl_pathv[1] = "-f";
execv("/bin/mv", &globbuf.gl_pathv[0]);
// ...
glob_t globbuf2;
globbuf2.gl_offs = 2;
glob(airmv, GLOB_DOOFFS, NULL, &globbuf2);
glob(air, GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf2);
globbuf2.gl_pathv[0] = "mv";
globbuf2.gl_pathv[1] = "-f";
execv("/bin/mv", &globbuf2.gl_pathv[0]);
// ...
char *c[] = {"rm", "-rf", animal, NULL};
execv("/usr/bin/rm", c);
```

Penjelasan: 
- `glob` digunakan untuk mencari nama file yang sesuai dengan pola.
- `daratmv` dan `airmv` masing-masing berisi `home/[USER]/modul2/*darat*` dan `home/[USER]/modul2/*air*`. Tanda ** digunakan untuk mencari substring yang cocok pada nama file.
- `mv -f` digunakan untuk memindahkan file tanpa prompt.
- `rm -rf` digunakan untuk menghapus directory dan file didalamnya secara rekursif.

## Soal d
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory `/home/[USER]/modul2/darat`. Hewan burung ditandai dengan adanya `bird` pada nama file.

Code:
```
glob_t globbuf3;
globbuf3.gl_offs = 1;
glob(bird, GLOB_DOOFFS, NULL, &globbuf3);
globbuf3.gl_pathv[0] = "rm";
execv("/bin/rm", &globbuf3.gl_pathv[0]);
```

Penjelasan:
- `rm` digunakan untuk menghapus file.

## Soal e
Terakhir, Conan harus membuat file `list.txt` di folder `/home/[USER]/modul2/air` dan membuat list nama semua hewan yang ada di directory `/home/[USER]/modul2/air` ke `list.txt` dengan format `UID_[UID file permission]_Nama File.[jpg/png]` dimana UID adalah user dari file tersebut dan file permission adalah permission dari file tersebut. Contoh : `conan_rwx_hewan.png`

Code:
```
char *d[] = {"touch", list, NULL};
execv("/bin/touch", d);
// ...
execl("/bin/find", "find", air, "-name", "*.jpg", "-fprintf", list, "%u_%M_%f\n", NULL);
```
Penjelasan:
- `touch` digunakan untuk membuat file baru.
- `find` digunakan untuk mencari file.
- `-name *.jpg` digunakan untuk melihat nama file dengan format .jpg.
- `fprintf` berarti mencetak hasilnya ke dalam file.
- `%u` mengambil nama user, `%M` mengambil permission, dan `%f` mengambil nama file tersebut. 

Dokumentasi hasil pengerjaan:

![3.1](https://gitlab.com/nha_14/dokumentasi-sisop/-/raw/main/img2/1.jpg)
![3.2](https://gitlab.com/nha_14/dokumentasi-sisop/-/raw/main/img2/2.jpg)
![3.3](https://gitlab.com/nha_14/dokumentasi-sisop/-/raw/main/img2/3.jpg)
![3.4](https://gitlab.com/nha_14/dokumentasi-sisop/-/raw/main/img2/4.jpg)
![3.5](https://gitlab.com/nha_14/dokumentasi-sisop/-/raw/main/img2/5.jpg)

Kendala yang dialami:
- Masih bingung dalam pembuatan khusus tiap perintah di dalam program c.
