#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <glob.h>
#include <stdio.h>
#include <time.h>
#include <dirent.h>
#include <fcntl.h>
#include <string.h>

int main(){
  pid_t child_id, child_id2, child_id3, child_id4, child_id5, child_id6, child_id7, child_id8;
  int status;
  
  char *user;
  user = (char *)malloc(10*sizeof(char));
  user = getlogin();
   
  char home[100]; 
  strcpy(home, "/home/");
  strcat(home, user);

  char modul2[100];
  strcpy(modul2, home); 
  strcat(modul2, "/modul2");

  char zip[100];
  strcpy(zip, home); 
  strcat(zip, "/animal.zip");

  char animal[100]; 
  strcpy(animal, modul2);
  strcat(animal, "/animal");

  char darat[100];
  strcpy(darat, modul2);
  strcat(darat, "/darat");

  char air[100];
  strcpy(air, modul2);
  strcat(air, "/air"); 

  char daratmv[100];
  strcpy(daratmv, animal);
  strcat(daratmv, "/*darat*");

  char airmv[100];
  strcpy(airmv, animal);
  strcat(airmv, "/*air*");

  char bird[100];
  strcpy(bird, darat);
  strcat(bird, "/*bird*");

  char list[100]; 
  strcpy(list, air); 
  strcat(list, "/list.txt");

  child_id = fork();

  if(child_id < 0){
    exit(EXIT_FAILURE);
  }

  if(child_id == 0){
    char *argv[] = {"mkdir", "-p", darat, NULL};
    execv("/bin/mkdir", argv);

  }else{
    while((wait(&status)) > 0);
    sleep(3);
    
    child_id2 = fork();
    if(child_id2 < 0){
      exit(EXIT_FAILURE);
    }

    if(child_id2 == 0){
      char *a[] = {"mkdir", "-p", air, NULL};
      execv("/bin/mkdir", a);

    }else{
      while((wait(&status)) > 0);
      child_id3 = fork();
      if(child_id3 < 0){
        exit(EXIT_FAILURE);
      }

      if(child_id3 == 0){
        char *b[] = {"unzip", "-q", zip, "-d", modul2, NULL};
        execv("/usr/bin/unzip", b);

      }else{
        while((wait(&status)) > 0);
        child_id4 = fork();
        if(child_id4 < 0){
          exit(EXIT_FAILURE);
        }

        if(child_id4 == 0){
          glob_t globbuf;
          globbuf.gl_offs = 2;
          glob(daratmv, GLOB_DOOFFS, NULL, &globbuf);
          glob(darat, GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf);
          globbuf.gl_pathv[0] = "mv";
          globbuf.gl_pathv[1] = "-f";
          execv("/bin/mv", &globbuf.gl_pathv[0]);

        }else{
          while((wait(&status)) > 0);
          child_id5 = fork();
          if(child_id5 < 0){
            exit(EXIT_FAILURE);
          }

          if(child_id5 == 0){
            glob_t globbuf2;
            globbuf2.gl_offs = 2;
            glob(airmv, GLOB_DOOFFS, NULL, &globbuf2);
            glob(air, GLOB_DOOFFS | GLOB_APPEND, NULL, &globbuf2);
            globbuf2.gl_pathv[0] = "mv";
            globbuf2.gl_pathv[1] = "-f";
            execv("/bin/mv", &globbuf2.gl_pathv[0]);
          
          }else{
            while((wait(&status)) > 0);
            child_id6 = fork();
            if(child_id6 < 0){
              exit(EXIT_FAILURE);
            }

            if(child_id6 == 0){
              char *c[] = {"rm", "-rf", animal, NULL};
              execv("/usr/bin/rm", c);
          
            }else{
              while((wait(&status)) > 0);
              child_id7 = fork();
              if(child_id7 < 0){
               exit(EXIT_FAILURE);
              }

              if(child_id7 == 0){
                glob_t globbuf3;
                globbuf3.gl_offs = 1;
                glob(bird, GLOB_DOOFFS, NULL, &globbuf3);
                globbuf3.gl_pathv[0] = "rm";
                execv("/bin/rm", &globbuf3.gl_pathv[0]);

              }else{
                while((wait(&status)) > 0);
                child_id8 = fork();
                if(child_id8 < 0){
                exit(EXIT_FAILURE);
                }

                if(child_id8 == 0){
                  char *d[] = {"touch", list, NULL};
                  execv("/bin/touch", d);

                }else{
                  while((wait(&status)) > 0);
                  execl("/bin/find", "find", air, "-name", "*.jpg", "-fprintf", list, "%u_%M_%f\n", NULL);
                }
              }
            }
          }
        }
      }
    }
  }
}
