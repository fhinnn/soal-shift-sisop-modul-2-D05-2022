#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>

void pertama() {
int status = 0;
pid_t child_id;

child_id = fork();

char *download[][8] = {{ "wget", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "-O", "Weapon.zip", NULL},{ "wget", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "-O", "Character.zip", NULL}};

char *ekstrak[][5] = {{ "unzip", "Weapon.zip", NULL},{"unzip", "Character.zip", NULL}};

if (child_id==0){
for (int i=0;i<2;i++){
if (fork()==0) continue;
execv ("/usr/bin/wget",download[i]);
}
}
else{
while ((wait(&status)) > 0);
for (int z=0;z<2;z++){
if (fork()==0) continue;
execv ("/usr/bin/unzip",ekstrak[z]);
}
}
}


int main() {
  pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child

    char *argv[] = {"mkdir", "-p", "gacha_gacha", NULL};
    execv("/bin/mkdir", argv);
}
}
